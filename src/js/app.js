"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import simpleParallax from 'simple-parallax-js';

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')


// Меню для мобильной версии
mobileNav();


if (document.querySelectorAll('[data-parallax]').length) {
    document.querySelectorAll('[data-parallax]').forEach(el => {
        new simpleParallax(el, {
            scale: 1.2,
            orientation: 'down'
        });
    });
}


document.addEventListener('click', (event) => {
    if(event.target.closest('[data-postnav-link]')) {
        const item = event.target.closest('[data-postnav-link]')
        const itemsArray = document.querySelectorAll('[data-postnav-link]')
        const chapter = item.getAttribute('href');

        itemsArray.forEach(elem => {
            elem.classList.remove('active')
        });
        item.classList.add('active')


    }
});

const smoothLinks = document.querySelectorAll('a[href^="#"]');
for (let smoothLink of smoothLinks) {
    smoothLink.addEventListener('click', function (e) {
        e.preventDefault();
        const id = smoothLink.getAttribute('href');

        document.querySelector(id).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
};

(() => {
    let stickyCollection = document.querySelectorAll('[data-postnav]');
    if(stickyCollection.length) {
        const headerHeight = document.querySelector('.header').clientHeight;
        let a = document.querySelector('[data-postnav]'), b = null, P = headerHeight;  // если ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента. Может быть отрицательным числом
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);
        function Ascroll() {
            if (b == null) {
                let Sa = getComputedStyle(a, ''), s = '';
                for (let i = 0; i < Sa.length; i++) {
                    if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                        s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
                    }
                }
                b = document.createElement('div');
                b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                a.insertBefore(b, a.firstChild);
                var l = a.childNodes.length;
                for (var i = 1; i < l; i++) {
                    b.appendChild(a.childNodes[1]);
                }
                a.style.height = b.getBoundingClientRect().height + 'px';
                a.style.padding = '0';
                a.style.border = '0';
            }
            let Ra = a.getBoundingClientRect(),
                R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('.footer').getBoundingClientRect().top + 0);  // селектор блока, при достижении верхнего края которого нужно открепить прилипающий элемент;  Math.round() только для IE; если ноль заменить на число, то блок будет прилипать до того, как нижний край элемента дойдёт до футера
            if ((Ra.top - P) <= 0) {
                if ((Ra.top - P) <= R) {
                    b.className = 'stop';
                    b.style.top = - R - 44 +'px';
                } else {
                    b.className = 'sticky';
                    b.style.top = P + 'px';
                }
            } else {
                b.className = '';
                b.style.top = '';
            }
            window.addEventListener('resize', function() {
                a.children[0].style.width = getComputedStyle(a, '').width
            }, false);
        }
    }

    window.addEventListener('resize', (event) => {
    //    console.log('resize');
        if (document.querySelector('[data-postnav] .sticky')) {
            document.querySelector('[data-postnav] > .sticky').classList.remove('sticky')
        }
    });
})()

// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

// Sliders
import "./components/sliders.js";


