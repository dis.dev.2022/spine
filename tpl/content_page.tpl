{extends 'file:common/_layout.tpl'}

{block 'main'}
    <main class="page">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-12 offset-sm-1 col-sm-10">
                        <h1 class="heading heading--line">
                            <span>{$_modx->resource.pagetitle}</span>
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-xl-2 offset-xl-1">
                        <nav class="post-nav" data-postnav>
                            <ul>
                                {$_modx->runSnippet('!pdoResources', [
                                    'sortby' => 'menuindex',
                                    'includeContent' => 1,
                                    'showUnpublished' => 1,
                                    'sortdir' => 'ASC',
                                    'tpl' => 'tpl.chapter.nav',
                                    'limit' => 20
                                ])}
                            </ul>
                        </nav>
                    </div>
                    <div class="col-12 offset-sm-1 col-sm-10 col-xl-8 offset-xl-0 col-xxl-7 col-xxxl-6">
                        <div class="post">
                            {$_modx->runSnippet('!pdoResources', [
                                'sortby' => 'menuindex',
                                'includeContent' => 1,
                                'showUnpublished' => 1,
                                'sortdir' => 'ASC',
                                'tpl' => 'tpl.chapter',
                                'limit' => 20
                            ])}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- .main -->
{/block}

{set $media_slider = json_decode($id | resource: 'slider', true)}
{if $media_slider}
    <div class="post__gallery post-gallery">
        <div class="swiper" data-gallery>
            <div class="swiper-wrapper">
                {foreach $media_slider as $row}
                    <div class="swiper-slide">
                        <a href="{$row.image}" class="post-gallery__item" data-fancybox>
                            <img src="{$row.image}" class="img-fluid" alt="">
                        </a>
                    </div>
                {/foreach}
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-nav swiper-nav--prev">
                <button class="btn-nav" data-gallery-prev>
                    <i>
                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/img/sprites/sprite.svg#arrow-left-long" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </button>
            </div>
            <div class="swiper-nav swiper-nav--next">
                <button class="btn-nav" data-gallery-next>
                    <i>
                        <svg class="ico-svg" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="assets/template/img/sprites/sprite.svg#arrow-right-long" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                </button>
            </div>
        </div>
    </div><!-- .post-gallery -->
{/if}
