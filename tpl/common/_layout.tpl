{set $id = $_modx->resource.id}
{set $title = ($_modx->resource.longtitle ?: $_modx->resource.pagetitle) | notags}
{set $description = $_modx->resource.description | replace :' "':' «' | replace :'"':'»'}
{set $page = 'site_url' | config ~ $_modx->resource.uri}

{var $lang = $_modx->config.cultureKey}


<!DOCTYPE html>
<html lang="ru">
<head>
    {block 'head'}
        <base href="{'site_url' | config}" />
        <meta charset="utf-8">
        <title>{$title} | {'site_name' | config}</title>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="{$description}">
        <meta name="keywords" content="">
        <meta name="robots" content="">

        <meta name="csrf-token" content="{$session['csrf-token']}">

        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

        <!-- Facebook Open Graph -->
        <meta property="og:url" content="{$page}">
        <meta property="og:type" content="website">
        <meta property="og:title" content="{$title}">
        <meta property="og:image" content="">
        <meta property="og:description" content="{$description}">
        <meta property="og:site_name" content="{'site_name' | config}">

        <!-- Schema.org -->
        <meta itemprop="name" content="{$title}">
        <meta itemprop="description" content="{$description}">
        <meta itemprop="image" content="">


        <link rel="stylesheet" href="assets/template/css/main.min.css">
    {/block}
</head>
<body>

<div class="root">

    {block 'header'}
        <header class="header {$id == 1 ? 'header--fixed' : ''}">
            <div class="container">
                <div class="wrapper">
                    <div class="row">
                        <div class="col-3 d-flex align-center offset-sm-1 col-lg-2">
                            <a href="/" class="header__logo">
                                <img src="assets/template/img/logo-needle.svg" class="img-fluid" alt="">
                            </a>
                        </div>
                        <div class="col-6 col-sm-4 col-lg-5 d-flex align-center">
                            <nav class="header__nav nav">
                                <div class="nav__wrap">
                                    {'pdoMenu' | snippet : [
                                        'startId' => 0,
                                        'level' => 1,
                                        'resources' => '-1',
                                        'tplOuter' => '@INLINE <ul class="nav__main" [[+classes]]>[[+wrapper]]</ul>',
                                        'firstClass' => 'nav-first',
                                        'lastClass' => 'nav-last',
                                    ]}
                                    <ul class="nav__lng">
                                        {if $lang == 'ru'}
                                            <li>
                                                <span>Ru</span>
                                            </li>
                                            <li>
                                                <a href="/en/">En</a>
                                            </li>
                                        {else}
                                            <li>
                                                <a href="/">Ru</a>
                                            </li>
                                            <li>
                                                <span>En</span>
                                            </li>
                                        {/if}
                                    </ul>
                                </div>
                            </nav>
                        </div>
                        <div class="col-3 d-flex justify-content-end">
                            <div class="header__button">
                                <a href="https://t.me/zontiktikkurilla" class="btn" target="_blank">ПОГОВОРИТЬ С НАМИ</a>
                            </div>
                            <div class="header__hamburger">
                                <button type="button" class="hamburger" data-nav-toggle>
                                    <span class="hamburger__inner"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- .header -->
    {/block}

    {block 'main'}
        <main class="page">
            <div class="container">
                <div class="wrapper">
                    <div class="row">
                        <div class="col-12 offset-sm-1 col-sm-10">
                            <h1 class="heading heading--line">
                                <span>{$_modx->resource.pagetitle}</span>
                            </h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 offset-sm-1 col-sm-10 offset-lg-3 col-lg-8 col-xl-7 col-xxl-6 col-xxxl-5">
                            <div class="capability-text ьи-0">
                                {$_modx->resource.content}</div><!-- .capability-text -->
                        </div>
                    </div>
                </div>
            </div>
        </main><!-- .main -->
    {/block}

    {block 'footer'}
        <footer class="footer footer--border">
            <div class="container">
                <div class="wrapper">
                    <div class="row">
                        <div class="col-12 col-sm-10  offset-sm-1">
                            <div class="footer__line"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-2 offset-sm-1 col-lg-2">
                            <div class="footer__copy">© 2022</div>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-0  col-lg-3">
                            <ul class="footer__links">
                                <li class="footer__links__item">
                                    <a href="mailto:{'email' | config}" class="footer__link footer__link--email">
                                        <i>
                                            <img src="assets/template/img/email-icon.svg" class="ico-svg" alt="">
                                        </i>
                                        <span>{'email' | config}</span>
                                    </a>
                                </li>
                                <li class="footer__links__item">
                                    <a href="{'telegram_link' | config}" class="footer__link footer__link--telegram">
                                        <i>
                                            <img src="assets/template/img/telegram-icon.svg" class="ico-svg" alt="">
                                        </i>
                                        <span>{'telegram' | config}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-0 col-lg-3 offset-lg-2">
                            <div class="footer__button">
                                <a href="https://t.me/zontiktikkurilla" class="btn" target="_blank">ПОГОВОРИТЬ С НАМИ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer><!-- .footer -->
    {/block}

</div>

{block 'modal'}
    {include 'file:common/modal.tpl'}
{/block}

{block 'scripts'}
    <script src="assets/template/js/app.min.js"></script>
{/block}

</body>
</html>
