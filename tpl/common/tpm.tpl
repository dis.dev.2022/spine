<p style="text-align: center;"><img style="border-style: double;" src="assets/media/ai-instead.jpg" alt="" width="600" height="338" /></p>
<p style="text-align: center;"><sub>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<sup>превью одного из видео с канала</sup></sub></p>
<p style="text-align: center;"><span style="font-size: 36pt;">_________________________________________________________</span></p>
<p style="text-align: center;"><span style="font-size: 36pt;">ЦЕЛИ<sup>*</sup></span></p>
<p style="text-align: center;"><span style="font-size: 36pt;">_________________________________________________________</span></p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><sup>*Для иллюстрации этого кейса мы рассматриваем только первый год работы (вся статистика из YouTube Studio приведена за период с 6 апреля 2021 года по 6 апреля 2022 года)</sup></p>
<p style="text-align: center;"><sup>Цифры на <em>сегодняшний день</em> вы можете посмотреть <a title="Ссылка на YT канал" href="https://www.youtube.com/@Sumsubcom">пройдя по ссылке</a></sup></p>
<p>&nbsp; &nbsp; &nbsp;Главной целью основателей компании <a title="Ссылка на сайт компании Sumsub" href="https://sumsub.com" target="_blank" rel="noopener">Sumsub</a> была <a title=" Ссылка на Wiki" href="https://en.wikipedia.org/wiki/Brand_awareness" target="_blank" rel="noopener">brand awareness</a>,т.е. максимальная известность для компании. Сложность была в том, что компания продает b2b продукт, что обычным людям <em>не интересно</em>.</p>
<p>&nbsp; &nbsp; &nbsp;Итак, мы должны были:</p>
<ul>
    <li>привлечь <em>правильную</em> аудиторию (из сферы деятельности Sumsub);</li>
    <li>кристально ясно довести до аудитории кто такие Sumsub и чем они занимаются;</li>
    <li>сделать англоязычный канал с прицелом на страны Азии и Европы;</li>
    <li>стать максимально интересными для целевой аудитории, чтобы заработали алгоритмы YT;</li>
    <li>дать много пользы, чтобы подписка быстро органически росла;</li>
</ul>
<p>&nbsp; &nbsp; Обсудив с фаундерами &nbsp;, мы поставили цель привлечь на канал живую целевую аудиторию <strong>50 000 человек за 1 год.</strong></p>
<p><strong>&nbsp; &nbsp;&nbsp;</strong>Мы стартовали 6 апреля 2021 года</p>
<p><strong>&nbsp; &nbsp; </strong>В итоге, ровно через год, 6 апреля 2022 на канале было <strong>139,974 подписчика</strong> (сколько подписчиков сейчас <a title="Ссылка на канал Sumsub в YT" href="https://www.youtube.com/@Sumsubcom" target="_blank" rel="noopener">смотрите по ссылке</a>)</p>
<p>&nbsp; &nbsp; Наши видео посмотрели <strong>4 млн 553 тысячи 490 раз</strong></p>
<p>&nbsp; &nbsp; Охват составил <strong>86 миллионов человек</strong></p>
<p>&nbsp;</p>
<p><img style="border-style: double;" src="assets/media/views-sumsub.png" alt="" width="500" height="227" /></p>
<p><img src="assets/media/impr-sum.png" alt="" width="506" height="428" /></p>
<p>&nbsp;</p>
<p>А вот топ-5 стран, которые через год смотрели Sumsub. На первом месте США - главная англоязычная публика, далее Индия (наш канал про IT, а это страна поставщик "дешевых мозгов" для IT-сферы), ну и далее Европа, Северная Америка...</p>
<p><img style="border-style: double;" src="assets/media/top-countr-sum.png" alt="" width="1196" height="790" /></p>
<p>&nbsp;</p>
<p>А вот так выглядит картина по возрасту и полу. Понятно, что в тематике канале доминирующей аудиторией будет мужская. А двумя главными возрастными группами стали молодые люди от 18 до 24 и от 25 до 34.</p>
<p><img style="border-style: double;" src="assets/media/age-sum.png" alt="" width="1204" height="1178" /></p>
<p>Ну и один из самых важных параметров статистики.</p>
<p>Источники трафика.</p>
<p>На скриншоте ниже видно, что трафик органический, привлечен &nbsp;алгоритмами YouTube, никаких накруток и нечестных методов мы не использовали и никогда не используем.&nbsp;</p>
<p><img style="border-style: double;" src="assets/media/traffic-sum.png" alt="" width="1202" height="1080" /></p>
<p>&nbsp;</p>





<div class="row">
    <div class="col-lg-5">
        <div class="post-image post-image-md">
            <img src="assets/template/img/chart_01.jpg" alt="">
        </div>
    </div>
    <div class="col-lg-7">
        <p>В итоге, ровно через год, 6 апреля 2022 на канале было <strong>139,974 подписчика</strong> (сколько подписчиков сейчас <a title="Ссылка на канал Sumsub в YT" href="https://www.youtube.com/@Sumsubcom" target="_blank" rel="noopener">смотрите по ссылке</a>)</p>

    </div>
</div>



