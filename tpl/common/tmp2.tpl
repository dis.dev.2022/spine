<p style="text-align: center;"><span style="font-size: 36pt;">_________________________________________________________</span></p>
<p style="text-align: center;"><span style="font-size: 36pt;">КАК МЫ ДОБИЛИСЬ РЕЗУЛЬТАТА?</span></p>
<p style="text-align: center;"><span style="font-size: 36pt;">_________________________________________________________</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Канал SumSub был запущен 6 апреля 2021 года. Давайте взглянем как шла работа. Первые 2 месяца мы работали &laquo;в стол&raquo; - просто выпускали видео по темам, которые мы определили в стратегии канала заранее.</p>
<p></p>
<p>Как видно на этом скриншоте с 6 апреля по 26 мая, то есть примерно за 2 месяца мы выпустили 6 видео и собрали 3900 просмотров и 216 подписчиков.</p>
<p>Обратите внимание на некоторые видео. Например, How to disappear completely and never be found</p>
<p><img src="assets/media/maxresdefault-3.jpg" alt="" width="600" height="338" /></p>
<p><img src="assets/media/navsegda.png" alt="" width="600" height="313" /></p>


<h2>КАК МЫ ДОБИЛИСЬ РЕЗУЛЬТАТА?</h2>
<p>Канал SumSub был запущен 6 апреля 2021 года. Давайте взглянем как шла работа. Первые 2 месяца мы работали &laquo;в стол&raquo; - просто выпускали видео по темам, которые мы определили в стратегии канала заранее.</p>
<div class="post-image">
    <img src="assets/media/do-26-maya.png" alt="" />
</div>
<p>Как видно на этом скриншоте с 6 апреля по 26 мая, то есть примерно за 2 месяца мы выпустили 6 видео и собрали 3900 просмотров и 216 подписчиков.</p>
<p>Обратите внимание на некоторые видео. Например, How to disappear completely and never be found</p>
<div class="post-image">
    <img src="assets/media/maxresdefault-3.jpg" alt="" />
</div>
<div class="post-image">
    <img src="assets/media/navsegda.png" alt="" />
</div>



<p>Или Using a Fake Passport to Pass Verification | Experiment</p>
<div class="post-image">
    <img src="assets/media/maxresdefault-4.jpg" alt="" />
</div>
<div class="post-image">
    <img src="assets/media/pasport.png" alt="" />
</div>
<p>На тот момент они набрали 473 просмотра и 302 просмотра соотвественно. Запомните эти цифры мы еще к ним вернемся.</p>
<p>Также давайте пристальней посмотрим на наши успехи по подписчикам за первые 2 месяца работы. 216 человек.</p>
<p>Но картина резко меняется 27 мая. Давайте посмотрим как. Для этого мы продлим нашу временную шкалу до конца 2021 года.</p>
