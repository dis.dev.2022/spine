{extends 'file:common/_layout.tpl'}

{block 'main'}
    <main class="page">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-12 offset-sm-1 col-sm-10">
                        <h1 class="heading">
                            <span>{$_modx->resource.pagetitle}</span>
                        </h1>
                        <div class="cases">
                            {$_modx->runSnippet('!pdoResources', [
                                'depth' => 0,
                                'limit' => 50,
                                'sortdir' => 'ASC',
                                'includeTVs' => 'case_image,case_video_webm,case_video_mp4,is_case_video',
                                'sortby' => 'menuindex',
                                'tpl' => 'tpl.case',
                            ])}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- .main -->
{/block}

{block 'footer'}
    <footer class="footer footer--base">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-12 col-md-2 offset-sm-1 col-lg-2">
                        <div class="footer__copy">© 2022</div>
                    </div>
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-0  col-lg-3">
                        <ul class="footer__links">
                            <li class="footer__links__item">
                                <a href="mailto:{'email' | config}" class="footer__link footer__link--email">
                                    <i>
                                        <img src="assets/template/img/email-icon.svg" class="ico-svg" alt="">
                                    </i>
                                    <span>{'email' | config}</span>
                                </a>
                            </li>
                            <li class="footer__links__item">
                                <a href="{'telegram_link' | config}" class="footer__link footer__link--telegram">
                                    <i>
                                        <img src="assets/template/img/telegram-icon.svg" class="ico-svg" alt="">
                                    </i>
                                    <span>{'telegram' | config}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-0 col-lg-3 offset-lg-2">
                        <div class="footer__button">
                            {if $lang == 'ru'}
                                <a href="#" class="btn">заказать продвижение</a>
                            {else}
                                <a href="#" class="btn">order promotion</a>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- .footer -->
{/block}
