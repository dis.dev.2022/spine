{extends 'file:common/_layout.tpl'}

{block 'main'}
    <main class="page">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-12 offset-sm-1 col-sm-10">
                        <h1 class="heading heading--line">
                            <span>{$_modx->resource.pagetitle}</span>
                        </h1>
                    </div>
                </div>
                <div class="capability">
                    <div class="row">
                        <div class="col-12 offset-sm-1 col-sm-10 offset-lg-3 col-lg-8 col-xl-7 col-xxl-6 col-xxxl-5">
                            <div class="capability-text">
                                {$_modx->resource.content}
                            </div><!-- .capability-text -->
                        </div>
                    </div>
                    <div class="row">
                        {set $rows = json_decode($_modx->resource.command, true)}
                        {foreach $rows as $row}
                            <div class="col-12 offset-sm-1 col-sm-10 col-lg-5">
                                <div class="user-item">
                                    <i class="user-item__media">
                                        <img src="{$row.image}" class="user-item__image" alt="">
                                    </i>
                                    <span class="user-item__content">
                                    <span class="user-item__title">{$row.set}</span>
                                    <span class="user-item__text">{$row.description}</span>
                                </span>
                                </div><!-- .user-item -->
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </main><!-- .main -->
{/block}
