{extends 'file:common/_layout.tpl'}

{block 'main'}
    <main class="main">
        <div class="main__wrap">

            <div class="promo-header">
                <div class="container">
                    <div class="wrapper">
                        <div class="row">
                            <div class="col-12 col-sm-10 offset-sm-1">
                                <h1 class="promo-heading">
                                    {$_modx->resource.introtext}
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .promo-header -->

            <div class="promo-media">
                <div class="container">
                    <div class="wrapper">
                        <div class="row">
                            <div class="col-12 col-sm-10 offset-sm-1 col-xxxl-9">
                                <div class="promo-media__wrap">
                                    <div class="promo-media__airship">
                                        <img src="assets/template/img/airship.png" class="img-fluid" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .promo-media -->

            <div class="mountains-one">
                <img src="assets/template/img/mountains_one.png" class="img-fluid" alt="">
            </div><!-- .mountains-one -->

            <div class="mountains-two">
                <div class="mountains-two__image">
                    <img src="assets/template/img/mountains_two.png" class="img-fluid" alt="">
                </div>
            </div><!-- .mountains-two -->

            <div class="promo-text promo-text--sm">
                <div class="container">
                    <div class="wrapper">
                        <div class="row">
                            <div class="col-12 col-sm-5 offset-sm-1 col-lg-8 offset-lg-3 col-xxl-6">
                                <div class="promo-text__wrap">
                                    {$_modx->resource.content}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main><!-- .main -->

    <div class="promo-text promo-text--lg">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-12 col-sm-5 offset-sm-1 col-lg-8 offset-lg-3 col-xxl-6">
                        <div class="promo-text__wrap">
                            {$_modx->resource.content}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}


{block 'footer'}
    <footer class="footer footer--home">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    {$_modx->runSnippet('!pdoResources', [
                        'parents' => 2,
                        'depth' => 1,
                        'resources' => '14,15,16,44',
                        'includeTVs' => 'case_hover,case_logo,subscriber_value,subscriber_label,views_value,views_label',
                        'sortby' => 'menuindex',
                        'tpl' => 'tpl.person',
                        'tplFirst' => 'tpl.person.first',
                        'tplLast' => 'tpl.person.last',
                        'limit' => 4
                    ])}
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-4 col-lg-3 offset-lg-0">
                        <ul class="footer__links footer__links--padding">
                            <li class="footer__links__item">
                                <a href="mailto:{'email' | config}" class="footer__link footer__link--email">
                                    <i>
                                        <img src="assets/template/img/email-icon.svg" class="ico-svg" alt="">
                                    </i>
                                    <span>{'email' | config}</span>
                                </a>
                            </li>
                            <li class="footer__links__item">
                                <a href="{'telegram_link' | config}" class="footer__link footer__link--telegram">
                                    <i>
                                        <img src="assets/template/img/telegram-icon.svg" class="ico-svg" alt="">
                                    </i>
                                    <span>{'telegram' | config}</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- .footer -->
{/block}

<div class="col-12 col-sm-3 offset-sm-1 col-md-2">
    <a href="{$id | url}" class="person">
        <div class="person__base">
            <div class="person__media">
                <div class="person__photo">
                    <img src="{$_pls['tv.case_logo']}" class="img-fluid" alt="{$pagetitle}">
                </div>
            </div>
            <div class="person__name">{$pagetitle}</div>
        </div>
        <div class="person__hover">
            <div class="person__layout">
                <div class="person__image">
                    <img src="{$_pls['tv.case_hover']}" class="img-fluid" alt="">
                </div>
                <div class="person__content">
                    <div class="person__group">
                        <div class="person__value">{$_pls['tv.subscriber_value']}</div>
                        <div class="person__label">{$_pls['tv.subscriber_label']}</div>
                    </div>
                    <div class="person__group">
                        <div class="person__value">{$_pls['tv.views_value']}</div>
                        <div class="person__label">{$_pls['tv.views_label']}</div>
                    </div>
                </div>
            </div>
        </div>
    </a><!-- .person -->
</div>
