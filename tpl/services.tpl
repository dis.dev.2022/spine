{extends 'file:common/_layout.tpl'}

{block 'main'}
    <main class="page">
        <div class="container">
            <div class="wrapper">
                <div class="row">
                    <div class="col-12 offset-sm-1 col-sm-10">
                        <h1 class="heading heading--line">
                            <span>{$_modx->resource.pagetitle}</span>
                        </h1>
                    </div>
                </div>
                <div class="capability">
                    <div class="row">
                        <div class="col-12 offset-sm-1 col-sm-10 offset-lg-3 col-lg-8 col-xl-7 col-xxl-6 col-xxxl-5">
                            <div class="capability-text">
                                {$_modx->resource.content}
                            </div><!-- .capability-text -->
                        </div>
                    </div>
                    <div class="row">
                        {$_modx->runSnippet('!pdoResources', [
                            'includeTVs' => 'item_image,is_dark',
                            'sortby' => 'menuindex',
                            'tpl' => 'tpl.service',
                            'tplOdd' => 'tpl.service.odd',
                            'limit' => 50
                        ])}
                    </div>
                </div>
            </div>
        </div>
    </main><!-- .main -->
{/block}
