// Получаем имя папки проекта
import * as nodePath from 'path';
const rootFolder = nodePath.basename(nodePath.resolve());

const buildFolder = `./build`; // Также можно использовать rootFolder
const srcFolder = `./src`;

export const path = {
    build: {
        js: `${buildFolder}/assets/template/js/`,
        css: `${buildFolder}/assets/template/css/`,
        html: `${buildFolder}/`,
        images: `${buildFolder}/assets/template/img/`,
        fonts: `${buildFolder}/assets/template/fonts/`,
        files: `${buildFolder}/assets/template/files/`
    },
    src: {
        js: `${srcFolder}/js/app.js`,
        images: `${srcFolder}/img/**/*.{jpg,jpeg,png,gif,webp}`,
        svg: `${srcFolder}/img/**/*.svg`,
        scss: `${srcFolder}/scss/main.scss`,
        html: `${srcFolder}/*.njk`,
        fonts: `${srcFolder}/assets/fonts/**/*.*`,
        files: `${srcFolder}/assets/files/**/*.*`,
        icons: `${srcFolder}/assets/icons/*.svg`,
    },
    watch: {
        js: `${srcFolder}/js/**/*.js`,
        scss: `${srcFolder}/scss/**/*.scss`,
        html: `${srcFolder}/**/*.njk`,
        images: `${srcFolder}/img/**/*.{jpg,jpeg,png,svg,gif,ico,webp}`,
        files: `${srcFolder}/assets/files/**/*.*`,
        icons: `${srcFolder}/assets/icons/*.svg`,
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder,
    ftp: ``
}
